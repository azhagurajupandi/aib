// Redirect dom
import React from "react";
import { Redirect } from "react-router-dom";


import Coins from '../src/views/Coins';
import Dashboard from '../src/views/Dashboard';

export default [
  {
    authNeeded: false,
    path: "/",
    exact: true,
    component: () => <Redirect to="/coins" />
  },
  {
    authNeeded: false,
    path: "/coins",
    component: Coins
  },
  {
    path: "/dashboard",
    component: Dashboard
  }  
];
