import { GET_COINS,GET_COINS_BY_ID} from '../action/types';

const initialState = {
    coinsList:[],
    coinData:{}
}

export default function(state = initialState, action:any) {
    switch (action.type) {
        case GET_COINS:
            return {
                ...state,
                coinsList:action.payload
            };  
        case GET_COINS_BY_ID:   
        return {
            ...state,
            coinData:action.payload
        };
        default:
            return state;
    }
}
