import { GET_COINS,GET_COINS_BY_ID } from "../action/types";
import fetch from '../utils/leoFetch'
//import axios from '../../../utils/leoAxios'
import { config } from "../config/config";


export const getCoinsList = () => (dispatch:any) => {
    console.log("Values in action");
    let options={}
    fetch(config.coinsListURL, options)
    .then(res => res.json())
    .then(coins =>
      dispatch({
        payload: coins,
        type: GET_COINS
      })
    );
  };

  export const getCoinsById = (coinsId:any) => (dispatch:any) => {
    console.log("Values in action coinsId",coinsId);
    //console.log(config.coinsListById + coinsId);
    let options = { }
    fetch(config.coinsListById + coinsId, options)
    .then(res => res.json())
    .then(coinsList =>
      dispatch({
        payload: coinsList,
        type: GET_COINS_BY_ID
      })
    );
  };