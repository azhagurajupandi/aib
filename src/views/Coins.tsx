import React from "react";
import {connect} from 'react-redux';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import {getCoinsList,getCoinsById} from '../action/getCoinsList';
import Global from '../global/global'
import {config} from '../config/config'
import { CLIENT_RENEG_LIMIT } from "tls";

interface MyProps{
    getCoinsList?:any,
    coinsList?:any,
    getCoinsById?:any,
    history?:any
}

interface MyState{
    page:any;
    rowsPerPage:any;
    setPage:any;
    setRowsPerPage:any;
    order:String;
    
}

// function descendingComparator(a:any, b:any, orderBy:any) {
//     if (b[orderBy] < a[orderBy]) {
//       return -1;
//     }
//     if (b[orderBy] > a[orderBy]) {
//       return 1;
//     }
//     return 0;
//   }

// function getComparator(order:any, orderBy:any) {
//     return order === 'desc'
//       ? (a:any, b:any) => descendingComparator(a, b, orderBy)
//       : (a:any, b:any) => -descendingComparator(a, b, orderBy);
//   }

function stableSort(array:any) {
    
    const stabilizedThis = array.map((el:any, index:any) => [el, index]);
    // stabilizedThis.sort((a:any, b:any) => {
    //   const order = comparator(a[0], b[0]);
    //   if (order !== 0) return order;
    //   return a[1] - b[1];
    // });
    return stabilizedThis.map((el:any) => el[0]);

  }

const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);


class Coins extends React.Component<MyProps,MyState> {
    constructor(props:any){
        super(props);
        this.state={
            page:0,
            rowsPerPage:10,
            setPage:0,
            setRowsPerPage:5,
            order:'asc',            
        }
    }

    componentWillMount(){
        this.props.getCoinsList();
    }

    handleChangePage = (event:any,newPage:any)=>{
        console.log("New Page",newPage)
        this.setState({
            page:newPage
        })
    }

    handleChangeRowsPerPage=(event:any)=>{
        let rowPage=parseInt(event.target.value,10);
        this.setState({
            rowsPerPage:rowPage,
            page:0
        })
    }

    handleClick=(event:any,id:any)=>{
        console.log("Idddddddddddddddddddd",id);
        this.props.getCoinsById(id);
        this.props.history.push(`\data`);
    }

  

    render() {
        console.log("coinsList", this.props.coinsList)
        let page=this.state.page;        
        let rowsPerPage=this.state.rowsPerPage;
        if(this.props.coinsList){
        return (           
            <Card className="card-head"> 
                <CardContent className="pg-inner-container">
                    <Typography gutterBottom variant="h5" component="h2" className="pg-title">Data Table</Typography>
                    <TableContainer component={Paper} className="emp-list">
                        <Table className="emp-table clone-req">
                            <TableHead>
                                <TableRow>
                                    <StyledTableCell>Id</StyledTableCell>
                                    <StyledTableCell>Image</StyledTableCell>
                                    <StyledTableCell>Name</StyledTableCell>
                                    <StyledTableCell>Symbol</StyledTableCell>
                                    <StyledTableCell>Current Price</StyledTableCell>
                                    <StyledTableCell>High 24 hour Price</StyledTableCell>
                                    <StyledTableCell>Low 24 hour Price</StyledTableCell>                            
                                </TableRow>
                            </TableHead>
                            <TableBody>
                            {stableSort(this.props.coinsList)
                            .slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage)
                            .map((row:any,index:any)=>{ 
                                const labelId = `enhanced-table-checkbox-${index}`;
                                // console.log("Page",this.state.page)
                                // console.log("RowsPerpage",this.state.rowsPerPage) 
                                // console.log("IN Next",row.STUDENT_NAME)                             
                               return(
                                   <TableRow
                                   hover
                                   key={row.id}
                                   onClick={event=>this.handleClick(event,row.id)}                                                                      
                                   >
                                    <TableCell>{row.id}</TableCell>
                                    <TableCell><img src={row.image} style={{ width: "20%"}}/></TableCell>
                                    <TableCell>{row.name}</TableCell>
                                    <TableCell>{row.symbol}</TableCell>
                                    <TableCell>{row.current_price}</TableCell>                                    
                                    <TableCell>{row.high_24h}</TableCell>
                                    <TableCell>{row.low_24h}</TableCell>
                                   </TableRow>
                               )
                            })
                                
                            }
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <TablePagination
                        rowsPerPageOptions={[10, 20, 30]}
                        component="div"
                        count={this.props.coinsList.length}
                        rowsPerPage={this.state.rowsPerPage}
                        page={this.state.page}
                        onChangePage={(event:any,newPage:any)=>this.handleChangePage(event,newPage)}
                        onChangeRowsPerPage={this.handleChangeRowsPerPage}
                    />
                </CardContent>
            </Card> 
        );
        }
    }
}
const mapStateToProps = (state:any) => ({
    coinsList:state.coins.coinsList
});
export default connect(mapStateToProps,{getCoinsList,getCoinsById})(Coins);
