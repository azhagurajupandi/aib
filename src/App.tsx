import React from 'react';
import {Provider} from 'react-redux';
import {Router, Route} from "react-router-dom";
import withTracker from "./withTracker";
import routes from "./routes";
import store from './store';
import Global from '../src/global/global';
import './App.css'


function App() {
  return (
    <Provider store={store}>
    <Router history={Global.history} >
        <div>
            {routes.map((route, index) => {
                let authFlag = route.authNeeded;
                if(authFlag===undefined) {
                    authFlag = true;
                }
                return (
                    <Route
                        key={index}
                        path={route.path}
                        exact={route.exact}
                        component={withTracker((props:any) => {
                        return (
                            <div>
                            
                                <route.component {...props}/>
                            
                            {authFlag}
                            </div>
                        );
                    })}/>
                );
            })}
        </div>
    </Router>
</Provider>
  );
}

export default App;
