import { createBrowserHistory } from 'history';
import {config} from '../config/config'

//const role = "";
export default class Global {
    static history= createBrowserHistory();

    static alertContent = {
        closable: true,
        message: "",
        status: 'info'
    }

    static clearAlert(){
        Global.alertContent = {
            closable: true,
            message: "",
            status: 'info'
        }
    }

    static isUserAuthenticated(){
        if (localStorage.getItem('token') 
        && localStorage.getItem('token') !== null 
        && localStorage.getItem('token') !== undefined){ 
        // && localStorage.getItem('token').trim() !== ''){
            return true;
        }
        else{
            return false;
        }
    }
    
    // static dashboardmenu () {
    //     if(Global.role==="System Admin"){
    //     return true;
    //     }
    //     else{
    //         return false;
    //     }
    //   }
    
}